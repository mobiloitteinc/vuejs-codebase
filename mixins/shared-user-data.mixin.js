export const SHARED_USER_DATA_MIXIN = {
  data() {
    return {
      registrationModel: {},
      isRegistrationCompleted: false,
      loginModel: {},
      pageType: this.$route.name,
      isForgotPasswordClicked: false,
      verification_token: null,
      forgotPasswordModel: {},
      registrationResponse:{},
      isContinueClicked: false,
      showModal: false,
      showMailSentLabel: false,
      resetPasswordModel:{},
      industries: [],
      buttonRedirectionPath:null,
      employeeNumberRange: [
        '1-9 Employees','10-49 Employees','50-99 Employees','100-249 Employees',
        '250-499 Employees','more than 500 Employees'
      ],
      jobcode:[{
        companyId: null,
        runTime: null
      }]
    }
  }
};
