// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './components/App'
import router from './router'
import Vuelidate from 'vuelidate'

import styles from './styles/styles.css'
import 'vue-material/dist/vue-material.css'
//import styleCss from 'assets/style.css'

import plugins from './config/plugins'
import theme from './config/theme'
import store from './store';
require('./assets/style.css')
Vue.use(Vuelidate)
// require('./modal-effect.js')
// require('./velocity.min.js')
// require('./3.1.1-jquery.min.js')
// require('./modal-effect.js')
// require('./bootstrap.min.js')
// require('./common.js')
// require('./velocity.ui.min.js')


plugins(Vue)
theme(Vue)

Vue.config.productionTip = false

/* eslint-disable no-new */
const vm = new Vue({
  router,
  store,
  ...App
})

vm.$mount('#app')

