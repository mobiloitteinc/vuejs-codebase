import axios from 'axios'

const registrationService={
    postCandidate(jObject){
        return new Promise((resolve, reject) => {
            axios.post('http://api.metru.at/v1/registration',jObject, { 'Content-Type': 'Application/Json' } )
            .then(response => {
                resolve(response.data)
            }).catch(e => {
                reject(e)
            })
        })
    },
    postRecruiter(jObject){
        return new Promise((resolve, reject) => {
            axios.post('http://api.metru.at/v1/registration/recruiter',jObject, { 'Content-Type': 'Application/Json' } )
            .then(response => {
                    resolve(response.data)
            }).catch(e => {
                    reject(e)
            })
        })
    },
    postNewactivationlink(email){
        let data = {'email': email};
        return new Promise((resolve, reject) => {
            axios.post('http://api.metru.at/v1/newactivationlink',data, { 'Content-Type': 'Application/Json' } )
            .then(response => {
                    resolve(response.data)
            }).catch(e => {
                    reject(e)
            })
        })
    },
    getIndustryList(){
        let culture='en'
        return new Promise((resolve, reject) => {
            axios.get('http://api.metru.at:8090/v1/industrylist/'+culture ,{ 'Content-Type': 'Application/Json' } )
            .then(response => {
                    resolve(response.data)
            }).catch(e => {
                    reject(e)
            })
        })
    }
}
export default registrationService
