import axios from 'axios'

const loginService={
    postLogin(jObject){
        return new Promise((resolve, reject) => {
            let username = jObject.Username;
            let password = jObject.Password;
            let credentials = btoa(username + ':' + password);
            let BasicAuth = 'Basic ' + credentials;
            axios({
                method: 'post',
                url: 'http://api.metru.at/login',/*
                params: {},*/
                headers: {
                  'Content-Type': 'application/json',
                  'Authorization': BasicAuth
                }
              })
            .then(response => {
                resolve(response.data)
            }).catch(e => {
                    reject(e)
            })
        })
    },
    postForgotPassword(email){
        return new Promise((resolve, reject)=>{
            axios({
              method: 'post',
              url: '/resetpassword',

            })
            .then(response=>{
                resolve(response.data)
            })
            .catch(e=>{
                reject(e)
            })
        })
    },

    getcandidateInfo (){
        return new Promise((resolve, reject) => {
            
            

            let credentials = window.localStorage.getItem('_verificationToken');
            if(credentials !=null && credentials !=undefined){
                let BasicAuth = 'Bearer ' + credentials;
                axios({
                    method: 'get',
                    url: 'http://api.metru.at/v1/profile/candidate/candidateinfo',
                    //data:jobcodeModel,
                    headers:{
                        'Content-Type': 'application/json',
                        'Authorization': BasicAuth,
                    }
                })
                .then(response => {
                    //alert("candidate_actiHello Settingson");
                    resolve(response.data)
                }).catch(e => {  
                        reject(e)
                })
            }
            else{
                this.$router.push('candidate');
            }
        })
    },
    postEditName(data,url){
        console.log(url+ "<=Hello Settings=>"+ JSON.stringify(data));
        return new Promise((resolve, reject) => {
            let credentials = window.localStorage.getItem('_verificationToken');
            if(credentials !=null && credentials !=undefined){
                let BasicAuth = 'Bearer ' + credentials;
                axios({
                    method: 'post',
                    url: 'http://api.metru.at:8090/v1/registration/updatename',
                    data: data,
                    headers:{
                        'Content-Type': 'application/json',
                        'Authorization': BasicAuth,
                    }
                })
                .then(response => {
                    resolve(response.data)
                }).catch(e => {  
                        reject(e)
                })
            }
            else{
                this.$router.push('candidate');
            }
        })
    },
    postEditEmail(data,url){
        console.log(url+ "<=Hello Settings=>"+ JSON.stringify(data));
        return new Promise((resolve, reject) => {
            let credentials = window.localStorage.getItem('_verificationToken');
            if(credentials !=null && credentials !=undefined){
                let BasicAuth = 'Bearer ' + credentials;
                axios({
                    method: 'post',
                    url: 'http://api.metru.at/v1/registration/candidate/updateemail',
                    data: data,
                    headers:{
                        'Content-Type': 'application/json',
                        'Authorization': BasicAuth,
                    }
                })
                .then(response => {
                    resolve(response.data)
                }).catch(e => {  
                        reject(e)
                })
            }
            else{
                this.$router.push('candidate');
            }
        })
    },
    postEditPassword(data,url){
        console.log(url+ "<=Hello Settings=>"+ JSON.stringify(data));
        return new Promise((resolve, reject) => {
            let credentials = window.localStorage.getItem('_verificationToken');
            if(credentials !=null && credentials !=undefined){
                let BasicAuth = 'Bearer ' + credentials;
                axios({
                    method: 'post',
                    url: 'http://api.metru.at/v1/registration/candidate/updateemail',
                    data: data,
                    headers:{
                        'Content-Type': 'application/json',
                        'Authorization': BasicAuth,
                    }
                })
                .then(response => {
                    resolve(response.data)
                }).catch(e => {  
                        reject(e)
                })
            }
            else{
                this.$router.push('candidate');
            }
        })
    }

}

export default loginService
