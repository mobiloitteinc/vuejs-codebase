import axios from 'axios'

const adminService = {
    getCompanyList(){
        return new Promise((resolve, reject) => {
            let credentials = window.localStorage.getItem('_verificationToken');
            if(credentials !==null && credentials !==undefined){
                let BasicAuth = 'Bearer ' + credentials;
                axios({
                    method: 'get',
                    url: 'http://api.metru.at/v1/admin/booking/listcompany',
                    headers:{
                        'Content-Type': 'Application/Json',
                        'Authorization': BasicAuth,
                    }
                })
                .then(response => {
                    resolve(response.data)
                }).catch(e => {
                    reject(e)
                })
            }
            else{
                this.$router.push('candidate');
            }
        })
    },
    getBookingList(){
        return new Promise((resolve, reject) => {
            let credentials = window.localStorage.getItem('_verificationToken');
            if(credentials !== null && credentials !== undefined){
                let BasicAuth = 'Bearer ' + credentials;
                axios({
                    method: 'get',
                    url: 'http://api.metru.at/v1/admin/booking/list',
                    headers:{
                        'Content-Type': 'Application/Json',
                        'Authorization': BasicAuth,
                    }
                })
                .then(response => {
                    resolve(response.data)
                }).catch(e => {
                    console.log(e)
                })
            }
            else{
                this.$router.push('candidate');
            }
        })
    },
    addJobcodes(jobCodeModel){
        return new Promise((resolve, reject) => {
            let credentials = window.localStorage.getItem('_verificationToken');
            if(credentials !== null && credentials !== undefined){
                let BasicAuth = 'Bearer ' + credentials;
                axios({
                    method: 'post',
                    url: 'http://api.metru.at/v1/admin/booking/create',
                    data: jobCodeModel,
                    headers:{
                        'Content-Type': 'application/json',
                        'Authorization': BasicAuth
                    }
                })
                .then(response => {
                    resolve(response.data)
                }).catch(e => {
                        reject(e)
                })
            }
            else{
                this.$router.push('candidate');
            }
        })
    }
}
export default adminService