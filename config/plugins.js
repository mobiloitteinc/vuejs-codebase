import VueMaterial from 'vue-material'
import VueRouter from 'vue-router'
import Vuelidate from 'vuelidate'
import VueVideoBg from 'vue-videobg'
import Vuex from 'vuex'
import 'vue-awesome/icons'

export default (Vue, ...params) => {
  params.filter(el => typeof el === 'object')
  .map(le => Vue.use(le))
  Vue.use(VueMaterial)
  Vue.use(VueRouter)
  Vue.use(Vuelidate)
  Vue.use(VueVideoBg)
  Vue.use(Vuex)
}
