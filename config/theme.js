const BaseTheme = {
  primary: {
    color: 'white',
    hue: 50
  },
  accent: {
    color: 'orange',
    hue: 800
  },
  warn: {
    color: 'red',
    hue: 400
  },
  background: {
    color: 'grey',
    hue: 0
  }
}

export default (Vue) => {
  Vue.material.registerTheme('default', BaseTheme)
}
