import Vue from 'vue'
import VueRouter from 'vue-router'

import Candidate from '@/components/register-candidate/register-candidate.component'
import Recruiter from '@/components/register-recruiter/register-recruiter.component'
import CandidateInfo from '@/components/candidate-dashboard/candidate-dashboard.component'
import RecruiterInfo from '@/components/recruiter-dashboard/recruiter-dashboard.component'
import ConfirmRegistrationInfo from '@/components/confirm-registration-info/confirm-registration-info.component'
import NewPasswordInfo from '@/components/new-password-info/new-password-info.component'
import ResetPasswordInfo from '@/components/reset-password-info/reset-password-info.component'
import AboutUsPage from '@/components/about-us/about-us.component'
import PrivacyPolicyPage from '@/components/privacy-policy/privacy-policy.component'
import ImprintPage from '@/components/imprint/imprint.component'
import TermsAndConditionsPage from '@/components/terms-and-conditions/terms-and-conditions.component'
import ForgotPasswordPage from  '@/components/forgot-password/forgot-password.component'
import NotFound from '@/components/not-found/NotFound'
import invitation from '@/components/invitation/invitation.component'
import adminPanel from '@/components/admin-panel/admin-panel.component'
/**/
import loginPage from '@/components/candidate-setting/candidate-setting.component'
import FAQPage from '@/components/FAQ/faq.component'
import CondidateShop from '@/components/Candidate-Shop/candidateshop.component'
import recruitersetting from '@/components/recruiter-setting/recruiter-setting.component'
import jobManagement from '@/components/job-management/job-management.component'
import candidateManagement from '@/components/candidate-management/candidate-management.component'

Vue.use(VueRouter)

const router = new VueRouter({
    routes: [
        { path: '/', redirect: '/candidate' },
        { path: '*', name:'not-found', component: NotFound },
        { path: '/candidate', name : 'candidate', component: Candidate },
        { path: '/recruiter', name : 'recruiter', component: Recruiter },
        { path: '/candidate-dashboard', name : 'candidate-dashboard', component: CandidateInfo },
        { path: '/recruiter-dashboard', name : 'recruiter-dashboard', component: RecruiterInfo },
        { path: '/confirm-registration-info', name : 'confirm-registration-info', component: ConfirmRegistrationInfo },
        { path: '/new-password-info', name : 'new-password-info', component: NewPasswordInfo },
        { path: '/reset-password-info', name : 'reset-password-info', component: ResetPasswordInfo },
        { path: '/about-us-page', name : 'about-us-page', component: AboutUsPage },
        { path: '/privacy-policy-page', name : 'privacy-policy-page', component: PrivacyPolicyPage },
        { path: '/terms-and-conditions-page', name : 'terms-and-conditions-page', component: TermsAndConditionsPage },
        { path: '/imprint-page', name : 'imprint-page', component: ImprintPage },
        { path: '/forgot-password', name : 'forgot-password', component: ForgotPasswordPage },
        { path: '/invitation-recruiter', name:'invitation-recruiter', component: invitation},
        { path: '/admin-panel', name:'admin-panel', component: adminPanel},
        /**/
        // Login code for candidate settings
        { path: '/candidate-setting', name: 'candidate-setting', component: loginPage },
        { path: '/FAQPage', name: 'FAQPage', component: FAQPage },
        { path: '/CondidateShop', name: 'CondidateShop', component: CondidateShop },
        { path: '/recruiter_setting', name: 'recruiter_setting', component: recruitersetting },
        { path: '/jobManagement', name: 'jobManagement', component: jobManagement },
        { path: '/candidateManagement', name: 'candidateManagement', component: candidateManagement },

    ]
})

export default router
