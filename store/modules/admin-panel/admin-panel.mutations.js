const mutations = {
    bookingList: (state, value)  => {
        state.bookingList = value;
    },
    companyList: (state, value) => {
        state.companyList = value;
    },
    showModalBooking: (state,value) => {
        state.showModalBooking = value;
    },
}

export default mutations

