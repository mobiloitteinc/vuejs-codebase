import adminService from '../../../services/admin.service'

const actions= {
    addJobCodes: ({ commit }, jobCodeModel) => {
        let jobCodeBooked = new Promise((resolve, reject) => {
            adminService.addJobcodes(jobCodeModel)
            .then(response => {
                resolve(response);
            }).then(() => {
              commit('bookingList')
            })
            .catch(e => {
                reject(e);
            })
        })
        return jobCodeBooked;
    },
    getCompanyList: ({ commit })  => {
        let companyListLoaded = new Promise((resolve, reject) => {
        adminService.getCompanyList()
          .then(response => {
            commit('companyList', response)
            resolve(response);
          }).catch(e => {
            reject(e);
        })
      })
      return companyListLoaded;
    },
    getBookingList: ({ commit }) => {
        let bookingListLoaded = new Promise ((resolve, reject) => {
            adminService.getBookingList()
            .then(response => {
                commit("bookingList", response)
                resolve(response);
            }).catch(e => {
                reject(e)
            })
        })
        return bookingListLoaded;
    },
    showModalBooking: ({ commit }, payload) => {
        commit('showModalBooking', payload);
    }
}

export default actions;
