import state from './admin-panel.state.js'
import getters from './admin-panel.getters.js'
import mutations from './admin-panel.mutations.js'
import actions from './admin-panel.actions.js'



export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}