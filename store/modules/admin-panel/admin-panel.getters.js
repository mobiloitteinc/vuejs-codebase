const getters = {
    bookingList: state => {
      return state.bookingList;
    },
    companyList: state => {
      return state.companyList;
    },
    showModalBooking: state => {
      return state.showModalBooking;
    },
}

export default getters;