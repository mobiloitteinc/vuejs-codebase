import loginService from '../../../services/login.service'
import candidateSetting from './candidate-setting.mutations'

const actions = {
   candidateInfo (context) {
        var promise = new Promise((resolve) => {
            loginService.getcandidateInfo()
            .then(response => {
              
                context.commit('loginPage', response);
                resolve(response);
            })
            .catch(e => {
                alert(e);
            })
          })
        return promise;
    },
    editNameInfo (context,data) {
        var promise = new Promise((resolve) => {
            loginService.postEditName(data)
            .then(response => {
              
                context.commit('loginPage', response);
                resolve(response);
            })
            .catch(e => {
                alert(e);
            })
          })
        return promise;
    },
    editEmailInfo (context,data) {
        var promise = new Promise((resolve) => {
            loginService.postEditEmail(data)
            .then(response => {
                context.commit('loginPage', response);
                resolve(response);
            })
            .catch(e => {
                alert(e);
            })
          })
        return promise;
    },
    editPasswordInfo (context,data) {
        var promise = new Promise((resolve) => {
            loginService.postEditPassword(data)
            .then(response => {
                context.commit('loginPage', response);
                resolve(response);
            })
            .catch(e => {
                alert(e);
            })
          })
        return promise;
    },
  }
export default actions