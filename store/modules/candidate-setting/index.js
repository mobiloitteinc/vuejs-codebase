import state from './candidate-setting.state.js'
import getters from './candidate-setting.getters.js'
import actions from './candidate-setting.actions.js'
import mutations from './candidate-setting.mutations.js'


export default {
    namespaced:true,
    getters,
    actions,
    state,
    mutations
  }
