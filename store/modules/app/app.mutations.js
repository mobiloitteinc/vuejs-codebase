const mutations = {
    isCandidate: (state, payload) => {
        state.isCandidate = payload;
    },
    isLoggedIn: (state, payload) => {
        state.isLoggedIn = payload;
    },
    isFooterLinks: (state, payload) => {
        state.isFooterLinks = payload;
    }
}

export default mutations
