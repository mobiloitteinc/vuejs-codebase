const actions = {
    checkUrl: ({ commit }, to, from) => {
      if(to.name === null || to.name === undefined){
        commit("isLoggedIn",true)
        return false
      }
      else if(to.name==='not-found' || to.name==='confirm-registration-info' || to.name==='new-password-info' || to.name==='reset-password-info' || to.name==='invitation-recruiter'){
          commit("isLoggedIn",true)
          return true
      }
      else{
        commit("isLoggedIn",false)
        return true
      }
    },
    checkLoginPages: ({ commit }, to) => {
      if(to==='forgot-password'){
        commit("isFooterLinks",false)
        return true;
      }
      else if(to==='candidate'){
        commit("isCandidate",true);
        commit("isFooterLinks",false);
        return true
      }
      else if(to==='recruiter')
      {
        commit("isCandidate",false);
        commit("isFooterLinks",false)
        return true
      }
      else if(to==='candidate-setting')
      {
        context.commit("isCandidate",false);
        context.commit("isFooterLinks",false)
        return true
      }
      else if(to==='recruiter_setting')
      {
        context.commit("isCandidate",false);
        context.commit("isFooterLinks",false)
        return true
      }
      else if(to==='about-us-page' || to==='privacy-policy-page' || to==='terms-and-conditions-page' || to==='imprint-page' || to==='invitation-recruiter'){
        commit("isFooterLinks",true)
        return true
      }
      else if(to==='not-found' || to==='confirm-registration-info' || to==='new-password-info' || to==='reset-password-info' || to==='invitation-recruiter'){
        commit("isLoggedIn",true)
        return true
      }
      else{
        return false;
      }
    }
  }

export default actions