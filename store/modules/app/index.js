import state from './app.state.js'
import getters from './app.getters.js'
import mutations from './app.mutations.js'
import actions from './app.actions.js'



export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}
