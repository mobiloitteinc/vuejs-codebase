const getters = {
    isLoggedIn: state => {
      return state.isLoggedIn
    },
    isCandidate: state => {
      return state.isCandidate
    },
    isFooterLinks: state => {
      return state.isFooterLinks
    }
}

export default getters;