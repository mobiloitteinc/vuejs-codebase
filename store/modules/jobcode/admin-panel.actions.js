import adminService from '../../../services/admin.service'
import adminPanelMutations from './admin-panel.mutations'

const actions={
    createJobCode (context, jobcodeModel){
        var promise = new Promise((resolve, reject) => {
            adminService.createJobcodes(jobcodeModel)
            .then(response => {
                resolve(response);
            })
            .catch(e => {
                reject(e);
            })
        })
        return promise;
    },
    getBookingList(context){
        var promise = new Promise ((resolve, reject) => {
            adminService.getBookingList()
            .then(response=>{
                context.commit("bookingList", response)
                resolve(response);
            }).catch(e => {
                reject(e)
            })
        })
        return promise;
    },
    showModal(context,value){
        context.commit('showModal',value);
    },
}

export default actions;
