import { state } from "./admin-panel.state";

const getters = {
    bookingList: state => state.bookingList,
    companyList: state => state.companyList,
    showModalJobCode : state => state.showModalJobCode,
}

export default getters;