import Vue from 'vue'
import Vuex from 'vuex'
import loginModule from './modules/login'
import registrationModule from './modules/registration'
import adminPanelModule from './modules/admin-panel'
import forgotPassword from './modules/forgot-password'
import candidateSetting from './modules/candidate-setting'
import appModule from './modules/app'

Vue.use(Vuex)

export const strict = false;

const debug = process.env.NODE_ENV !== 'development'

const store = new Vuex.Store({
  modules: {
    loginModule:loginModule,
    registrationModule : registrationModule,
    adminPanelModule : adminPanelModule,
    candidateSetting : candidateSetting,
    appModule : appModule,
  },
  strict: debug,
})
if (typeof window !== 'undefined') {
  document.addEventListener('DOMContentLoaded', function (event) {
    let expiration = window.localStorage.getItem('_verificationToken')
    let unixTimestamp = new Date().getTime() / 1000
    if (expiration !== null) {
      store.state.loginModule.isAuthenticated = true
    }
  })
}

export default store;